import Vue from 'vue'
import Router from 'vue-router'
import Scoreboard from '@/components/Scoreboard'
import TeamDetails from '@/components/TeamDetails'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Scoreboard',
      component: Scoreboard
    },
    {
      path: '/teams/:teamID',
      name: 'TeamDetails',
      component: TeamDetails,
      props: true
    }
  ]
})
