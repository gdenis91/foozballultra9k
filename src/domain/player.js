// @flow

import dynamicPlayerDetails from '../assets/dynamic-player-details'
import staticPlayerDetails from '../assets/static-player-details'
import {ESPNMapper, nflTeams} from './roster'
import _ from 'lodash'

interface StaticPlayerDetails {
  firstName:string;
  lastName:string;
  positionID:string;
  jerseyNumber:string;
  teamID:string;
}

interface WeeklyScore {
  totalScore:number;
  scoreByType:{[string]:number};
}

export default class Player {
  _staticDetails:StaticPlayerDetails
  _weeklyScores:WeeklyScore[]
  _playerID:string
  _rosterPosition:string

  constructor (playerID:string, staticDetails:StaticPlayerDetails, weeklyScores:WeeklyScore[]) {
    this._staticDetails = staticDetails
    this._weeklyScores = weeklyScores
    this._playerID = playerID
  }

  static NewPlayerFromID (playerID:string):Player {
    let dynamicDetails = dynamicPlayerDetails[playerID]
    let staticDetails = staticPlayerDetails[playerID]
    return new Player(playerID, staticDetails, dynamicDetails)
  }

  getPlayerID ():string {
    return this._playerID
  }

  getFirstName ():string {
    return this._staticDetails.firstName
  }

  getLastName ():string {
    return this._staticDetails.lastName
  }

  getDisplayName ():string {
    if (this.getLastName() === 'D/ST') {
      return 'D/ST'
    }
    return this.getFirstName().charAt(0) + '. ' + this.getLastName()
  }

  getJerseyNumber ():string {
    return this._staticDetails.jerseyNumber
  }

  getPositionID ():string {
    return this._staticDetails.positionID
  }

  getPositionName ():string {
    return ESPNMapper.PositionMapper(this.getPositionID())
  }

  getTeamID ():string {
    return this._staticDetails.teamID
  }

  getTeamName ():string {
    return nflTeams[this._staticDetails.teamID]
  }

  getPlayerInfo ():string {
    if (this.getLastName() === 'D/ST') {
      return this.getPositionName() + ', ' + this.getTeamName()
    }
    return this.getPositionName() + ', #' + this.getJerseyNumber() + ' - ' + this.getTeamName()
  }

  getTotalScore ():number {
    let totalScore = 0

    _.forEach(this._weeklyScores, (weeklyScoreDetails:WeeklyScore) => {
      totalScore += weeklyScoreDetails.totalScore
    })

    return _.round(totalScore, 2)
  }

  getScoreForWeek (week:number):number {
    let weeklyScore = _.nth(this._weeklyScores, week)
    return weeklyScore.totalScore
  }

  isScoringPlayer ():boolean {
    return ESPNMapper.IsScoringPosition(this.getPositionID())
  }

  setRosterPosition (position:string) {
    this._rosterPosition = position
  }

  getRosterPosition ():string {
    return this._rosterPosition
  }
}
