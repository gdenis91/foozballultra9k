// @flow

import staticTeamDetails from '../assets/static-team-details'
import Player from './player'
import {roster} from './roster'
import _ from 'lodash'

interface StaticTeamDetails {
  friendlyName:string;
  teamName:string;
  draftedPlayerIDs:Array<string>;
}

export default class Team {
  _staticDetails:StaticTeamDetails
  _teamID:string

  constructor (teamID:string, staticDetails:StaticTeamDetails) {
    this._staticDetails = staticDetails
    this._teamID = teamID
  }

  static NewTeamFromID (teamID:string):Team {
    let staticDetails = staticTeamDetails[teamID]
    return new Team(teamID, staticDetails)
  }

  static GetAllTeams ():Array<Team> {
    let allTeams = []

    _.forEach(staticTeamDetails, (details, teamID:string) => {
      allTeams.push(Team.NewTeamFromID(teamID))
    })

    return allTeams
  }

  getTeamID ():string {
    return this._teamID
  }

  getOwnerName ():string {
    return this._staticDetails.friendlyName
  }

  getTeamName ():string {
    return this._staticDetails.teamName
  }

  getPlayers ():Array<Player> {
    let players = []
    _.forEach(this._staticDetails.draftedPlayerIDs, (playerID:string) => {
      players.push(Player.NewPlayerFromID(playerID))
    })
    return players
  }

  getScoringPlayers ():Array<Player> {
    let players = []
    _.forEach(this._staticDetails.draftedPlayerIDs, (playerID:string) => {
      let player = Player.NewPlayerFromID(playerID)
      if (player.isScoringPlayer()) {
        players.push(player)
      }
    })
    return players
  }

  getTotalScore ():number {
    let totalScore = 0

    _.forEach(this.getScoringPlayers(), (player:Player) => {
      totalScore += player.getTotalScore()
    })

    return _.round(totalScore, 2)
  }

  getBestRosterForWeek (week:number):Array<Player> {
    let bestRoster = []

    let playersSortedByScore = _.sortBy(this.getPlayers(), (player: Player) => {
      return -player.getScoreForWeek(week)
    })

    let usedPlayerIDs = {}

    _.forEach(roster, (rosterPosition) => {
      for (let i = 0; i < rosterPosition.rosterQuantity; i++) {
        _.forEach(playersSortedByScore, (player:Player) => {
          if (usedPlayerIDs[player.getPlayerID()]) {
            return
          }
          if (_.includes(rosterPosition.eligiblePositionIDs, player.getPositionID())) {
            usedPlayerIDs[player.getPlayerID()] = true
            player.setRosterPosition(rosterPosition.abbreviation)
            bestRoster.push(player)
            return false
          }
        })
      }
    })

    // Add the remaining players to the bench
    _.forEach(playersSortedByScore, (player:Player) => {
      if (usedPlayerIDs[player.getPlayerID()]) {
        return
      }
      player.setRosterPosition('BENCH')
      bestRoster.push(player)
    })

    return bestRoster
  }

  getBestRosterScoreForWeek (week:number):number {
    let bestRoster = this.getBestRosterForWeek(week)
    let totalScore = 0
    _.forEach(bestRoster, (player:Player) => {
      if (player.getRosterPosition() !== 'BENCH') {
        totalScore += player.getScoreForWeek(week)
      }
    })
    return totalScore
  }

  getBestTotalScore ():number {
    let total = 0
    for (let i = 0; i < 13; i++) {
      total += this.getBestRosterScoreForWeek(i)
    }
    return _.round(total, 2)
  }

  getBestTotalScoreToWeek (week:number):number {
    let total = 0
    for (let i = 0; i <= week; i++) {
      total += this.getBestRosterScoreForWeek(i)
    }
    return _.round(total, 2)
  }
}
