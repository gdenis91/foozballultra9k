// @flow

import _ from 'lodash'

// 0 based week
export let currentWeek = 8

export let nflTeams = {
  '1': 'Falcons',
  '2': 'Bills',
  '3': 'Bears',
  '4': 'Bengals',
  '5': 'Browns',
  '6': 'Cowboys',
  '7': 'Broncos',
  '8': 'Lions',
  '9': 'Packers',
  '10': 'Titans',
  '11': 'Colts',
  '12': 'Chiefs',
  '13': 'Raiders',
  '14': 'Rams',
  '15': 'Dolphins',
  '16': 'Vikings',
  '17': 'Patriots',
  '18': 'Saints',
  '19': 'Giants',
  '20': 'Jets',
  '21': 'Eagles',
  '22': 'Cardinals',
  '23': 'Steelers',
  '24': 'Chargers',
  '25': '49ers',
  '26': 'Seahawks',
  '27': 'Buccaneers',
  '28': 'Redskins',
  '29': 'Panthers',
  '30': 'Jaguars',
  '31': '',
  '32': '',
  '33': 'Ravins',
  '34': 'Texans'
}

export let roster = {
  'QB': {
    abbreviation: 'QB',
    eligiblePositionIDs: ['1'],
    rosterQuantity: 1
  },
  'RB': {
    abbreviation: 'RB',
    eligiblePositionIDs: ['2'],
    rosterQuantity: 2
  },
  'WR': {
    abbreviation: 'WR',
    eligiblePositionIDs: ['3'],
    rosterQuantity: 2
  },
  'TE': {
    abbreviation: 'TE',
    eligiblePositionIDs: ['4'],
    rosterQuantity: 1
  },
  'FLX': {
    abbreviation: 'FLX',
    eligiblePositionIDs: ['2', '3', '4'],
    rosterQuantity: 1
  },
  'D/ST': {
    abbreviation: 'D/ST',
    eligiblePositionIDs: ['16'],
    rosterQuantity: 1
  },
  'DP': {
    abbreviation: 'DP',
    eligiblePositionIDs: ['11', '13'],
    rosterQuantity: 0
  },
  'K': {
    abbreviation: 'K',
    eligiblePositionIDs: ['5'],
    rosterQuantity: 1
  }
}

const positions = {
  'QB': {
    friendlyName: 'Quarterback',
    abbreviation: 'QB',
    identifyPositions: ['1'],
    isScored: true
  },
  'RB': {
    friendlyName: 'Running Back',
    abbreviation: 'RB',
    identifyPositions: ['2'],
    isScored: true
  },
  'WR': {
    friendlyName: 'Wide Receiver',
    abbreviation: 'WR',
    identifyPositions: ['3'],
    isScored: true
  },
  'TE': {
    friendlyName: 'Tight End',
    abbreviation: 'TE',
    identifyPositions: ['4'],
    isScored: true
  },
  'DP': {
    friendlyName: 'Defensive Player',
    abbreviation: 'DP',
    identifyPositions: ['11', '13']
  },
  'D/ST': {
    friendlyName: 'Defense/Special Teams',
    abbreviation: 'D/ST',
    identifyPositions: ['16']
  },
  'K': {
    friendlyName: 'Kicker',
    abbreviation: 'K',
    identifyPositions: ['5'],
    isScored: true
  }
}

export class ESPNMapper {
  static PositionMapper (positionID:string):string {
    let abbreviation = '???'
    _.forEach(positions, (position) => {
      if (_.includes(position.identifyPositions, positionID)) {
        abbreviation = position.abbreviation
      }
    })
    return abbreviation
  }

  static IsScoringPosition (positionID:string):boolean {
    let isScored = false
    _.forEach(positions, (position) => {
      if (_.includes(position.identifyPositions, positionID)) {
        isScored = position.isScored
      }
    })
    return isScored
  }

  static GetRosterPosition (rosterID:string) {
    return roster[rosterID]
  }
}
